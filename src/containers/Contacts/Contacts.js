import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteContact, editContact, fetchContact, fetchContacts} from "../../store/actions/buildingContacts";
import Spinner from "../../components/UI/Spinner/Spinner";
import Modal from "../../components/UI/Modal/Modal";
import FullContact from "../../components/FullContact/FullContact";
import Contact from "../../components/Contact/Contact";

class Contacts extends Component {
    state = {
        show: false
    };

    componentDidMount = async () => {
        await this.props.loadingContacts();
    };

    modalShow = (contactId) => {
        this.props.displayContact(contactId);
        this.setState({show: true});

    };
    modalHide = () => {
        this.setState({show: false});
    };

    removeContact = (contactId) => {
        this.props.deleteContact(contactId);
        this.setState({show: false});
    };

    render() {
        return (
            <Fragment>
                <div>
                    {this.props.contacts ? this.props.loading ? <Spinner/> :
                        Object.keys(this.props.contacts).map(id => (
                            <Contact
                                onClick={this.modalShow}
                                key={id}
                                photo={this.props.contacts[id].photo}
                                name={this.props.contacts[id].name}
                                id={id}
                            />
                        ))
                        : null}
                </div>
                {this.props.contact ?
                    <Modal show={this.state.show} close={this.modalHide}>
                        {this.props.loadingModal ? <Spinner/> :
                            <div>
                                <p onClick={this.modalHide} style={{textAlign:"right", fontSize:"25px", cursor:"pointer"}}><i className="fas fa-times"/></p>
                                <FullContact
                                    deleteContact={this.removeContact}
                                    contact={this.props.contact}
                                />
                            </div>
                        }
                    </Modal> : null
                }
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.contacts.contacts,
        loading: state.contacts.loading,
        error: state.contacts.error,
        contact: state.contacts.contact,
        loadingModal: state.contacts.loadingModal
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadingContacts: () => dispatch(fetchContacts()),
        displayContact: (contactId) => dispatch(fetchContact(contactId)),
        deleteContact: (contactId) => dispatch(deleteContact(contactId)),
        editContacts: () => dispatch(editContact()),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Contacts);