import React, {Component} from 'react';
import {Button, CardImg, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {editContact, fetchContact, postContact} from "../../store/actions/buildingContacts";

class AddAndEditContact extends Component {
    state = {
        name: "",
        phoneNumber: "",
        email: "",
        photo: "",
    };
    buttonTitle = this.props.match.params.id ? "Edit" : "Add";
    componentDidMount = async () => {
        if (this.props.match.params.id) {
            await this.props.fetchContact(this.props.match.params.id);
            this.setState({
                name: this.props.contact.name,
                phoneNumber: this.props.contact.phoneNumber,
                photo: this.props.contact.photo,
                email: this.props.contact.email
            });
        }
    };

    returnToTheMainPage = () => {
        this.props.history.push("/");
    };

    sendingNewContact = async (event) => {
        event.preventDefault();
        const contact = {
            name: this.state.name,
            phoneNumber: this.state.phoneNumber,
            photo: this.state.photo,
            email: this.state.email
        };
        if (this.props.match.params.id) {
            await this.props.editContact(this.props.match.params.id, contact, this.props.history);
        } else {
            await this.props.postContact(contact, this.props.history);
        }
    };
    valueChanged = event => this.setState({[event.target.name]: event.target.value});

    render() {
        return (
            <Form onSubmit={this.sendingNewContact}>
                <FormGroup>
                    <Label for="exampleName">Name</Label>
                    <Input type="text"
                           name="name"
                           id="exampleName"
                           value={this.state.name}
                           onChange={this.valueChanged}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="examplePhoneNumber">Phone number</Label>
                    <Input type="tel"
                           name="phoneNumber"
                           id="examplePhoneNumber"
                           value={this.state.phoneNumber}
                           onChange={this.valueChanged}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleEmail">Email</Label>
                    <Input type="email"
                           name="email"
                           id="exampleEmail"
                           value={this.state.email}
                           onChange={this.valueChanged}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="examplePhoto">Photo</Label>
                    <Input type="text"
                           name="photo"
                           id="examplePhoto"
                           value={this.state.photo}
                           onChange={this.valueChanged}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="examplePhotoPreview">Photo preview</Label>
                    <CardImg style={{margin: "20px", width: "100px"}} top width="100%" src={this.state.photo}/>
                </FormGroup>
                <Button style={{marginRight:"15px"}}>{this.buttonTitle}</Button>
                <Button onClick={this.returnToTheMainPage}>Back to contacts</Button>
            </Form>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.contacts.loading,
        error: state.contacts.error,
        contact: state.contacts.contact
    };
};

const mapDispatchToProps = dispatch => {
    return {
        postContact: (contact, h) => dispatch(postContact(contact, h)),
        editContact: (contactId, contact, h) => dispatch(editContact(contactId, contact, h)),
        fetchContact: (contactId) => dispatch(fetchContact(contactId))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(AddAndEditContact);