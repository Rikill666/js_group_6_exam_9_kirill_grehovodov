import axios from "axios";

const axiosContacts = axios.create({
    baseURL: 'https://homework71-d87c7.firebaseio.com/'
});

export default axiosContacts;