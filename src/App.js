import React from 'react';
import {Switch, Route} from "react-router-dom";
import {Container} from "reactstrap";
import Layout from "./components/Layout/Layout";
import Contacts from "./containers/Contacts/Contacts";
import AddAndEditContact from "./containers/AddAndEditContact/AddAndEditContact";


const App = () => {
  return (
      <Container>
        <Layout>
          <Switch>
            <Route path="/" exact component={Contacts}/>
            <Route path="/new" exact component={AddAndEditContact}/>
            <Route path="/edit/:id" exact component={AddAndEditContact}/>
            <Route render={() => <h1>Not Found</h1>}/>
          </Switch>
        </Layout>
      </Container>
  );
};

export default App;