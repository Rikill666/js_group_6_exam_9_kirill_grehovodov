import React from 'react';
import {Container, Navbar, NavbarBrand} from "reactstrap";
import {NavLink as NavRoute} from "react-router-dom";
import NavigationItems from "../NavigationItems/NavigationItems";

const Toolbar = () => {
    return (
        <Navbar>
            <Container>
                <NavbarBrand style={{background:'white'}} tag={NavRoute} to="/" exact>Contacts</NavbarBrand>
                <NavigationItems/>
            </Container>
        </Navbar>
    );
};

export default Toolbar;