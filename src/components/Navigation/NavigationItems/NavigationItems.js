import React from 'react';
import {Button, Nav} from "reactstrap";
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <Nav >
            <NavigationItem to="/new" exact style={{textAlign: "right"}}>
                <Button>Add new contact</Button>
            </NavigationItem>
        </Nav>
    );
};

export default NavigationItems;