import React from 'react';
import {Card, CardBody, CardImg, CardTitle, Col, Row} from "reactstrap";

const Contact = (props) => {
    return (
        <Card onClick={() => props.onClick(props.id)} style={{marginBottom: "10px", width:"75%", cursor:"pointer"}}>
            <Row>
                <Col>
                    <CardImg style={{margin: "20px", width: "100px"}} top width="100%" src={props.photo}
                             alt={props.name}/>
                </Col>
                <CardBody>
                    <Row style={{marginTop: "35px", fontWeight:"bold"}}>
                        <Col>
                            <CardTitle style={{fontSize:"30px"}}>{props.name}</CardTitle>
                        </Col>
                    </Row>
                </CardBody>
            </Row>
        </Card>
    );
};

export default Contact;