import React from 'react';
import {Button, ButtonGroup, Card, CardBody, CardImg, CardText, CardTitle, Col, Row} from "reactstrap";
import {NavLink} from "react-router-dom";

const FullContact = ({contact,deleteContact}) => {
    return (
        <div>
            <Card>
                <Row>
                    <Col>
                        <CardImg top width="100%" src={contact.photo} alt="Card image cap"/>
                    </Col>
                    <Col>
                        <CardBody style={{marginTop:"30px"}}>
                            <CardTitle><h2>{contact.name}</h2></CardTitle>
                            <CardText><i className="fas fa-phone-alt"/> {contact.phoneNumber}</CardText>
                            <CardText><i className="fas fa-envelope"/> {contact.email}</CardText>
                        </CardBody>
                    </Col>
                </Row>
                <ButtonGroup style={{margin:"15px"}}>
                    <Button tag={NavLink} to={"edit/" + contact.contactId } exact ><i
                        className="fas fa-pencil-alt"/> Edit</Button>
                    <Button onClick={()=>deleteContact(contact.contactId)}><i className="far fa-trash-alt"/> Delete</Button>
                </ButtonGroup>
            </Card>
        </div>
    );
};

export default FullContact;