import {

} from "../actions/actionsTypes";
import {FETCH_CONTACTS_REQUEST} from "../actions/actionsTypes";
import {FETCH_CONTACTS_SUCCESS} from "../actions/actionsTypes";
import {FETCH_CONTACTS_ERROR} from "../actions/actionsTypes";
import {FETCH_CONTACT_REQUEST} from "../actions/actionsTypes";
import {FETCH_CONTACT_SUCCESS} from "../actions/actionsTypes";
import {FETCH_CONTACT_ERROR} from "../actions/actionsTypes";
import {ADD_CONTACT_REQUEST} from "../actions/actionsTypes";
import {ADD_CONTACT_SUCCESS} from "../actions/actionsTypes";
import {ADD_CONTACT_ERROR} from "../actions/actionsTypes";
import {DELETE_CONTACT_REQUEST} from "../actions/actionsTypes";
import {DELETE_CONTACT_SUCCESS} from "../actions/actionsTypes";
import {DELETE_CONTACT_ERROR} from "../actions/actionsTypes";
import {PUT_CONTACT_REQUEST} from "../actions/actionsTypes";
import {PUT_CONTACT_SUCCESS} from "../actions/actionsTypes";
import {PUT_CONTACT_ERROR} from "../actions/actionsTypes";

const initialState = {
    contacts: null,
    error: null,
    loading: false,
    loadingModal: false,
    contact: null
};

const contactsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONTACTS_REQUEST:
            return {...state, loading: true};
        case FETCH_CONTACTS_SUCCESS:
            return {...state, contacts: action.contacts, error:null, loading: false};
        case FETCH_CONTACTS_ERROR:
            return {...state,error:action.error, loading: false};

        case FETCH_CONTACT_REQUEST:
            return {...state, loadingModal:true};
        case FETCH_CONTACT_SUCCESS:
            return {...state, contact: action.contact, error:null, loadingModal: false};
        case FETCH_CONTACT_ERROR:
            return {...state, error:action.error, loadingModal: false};

        case ADD_CONTACT_REQUEST:
            return {...state, loading: true};
        case ADD_CONTACT_SUCCESS:
            return {...state, error: null, loading: false};
        case ADD_CONTACT_ERROR:
            return {...state, error: action.error, loading: false};

        case PUT_CONTACT_REQUEST:
            return {...state, loading: true};
        case PUT_CONTACT_SUCCESS:
            return {...state, error: null, loading: false};
        case PUT_CONTACT_ERROR:
            return {...state, error: action.error, loading: false};

        case DELETE_CONTACT_REQUEST:
            return {...state, loading: true};
        case DELETE_CONTACT_SUCCESS:
            return {...state, error: null, loading: false};
        case DELETE_CONTACT_ERROR:
            return {...state, error: action.error, loading: false};
        default:
            return state;
    }
};



export default contactsReducer;