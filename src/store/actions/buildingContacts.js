import {

} from "./actionsTypes";
import axiosContacts from "../../axiosContacts";
import {FETCH_CONTACTS_REQUEST} from "./actionsTypes";
import {FETCH_CONTACTS_SUCCESS} from "./actionsTypes";
import {FETCH_CONTACTS_ERROR} from "./actionsTypes";
import {FETCH_CONTACT_REQUEST} from "./actionsTypes";
import {FETCH_CONTACT_SUCCESS} from "./actionsTypes";
import {FETCH_CONTACT_ERROR} from "./actionsTypes";
import {ADD_CONTACT_REQUEST} from "./actionsTypes";
import {ADD_CONTACT_SUCCESS} from "./actionsTypes";
import {ADD_CONTACT_ERROR} from "./actionsTypes";
import {DELETE_CONTACT_ERROR} from "./actionsTypes";
import {DELETE_CONTACT_SUCCESS} from "./actionsTypes";
import {DELETE_CONTACT_REQUEST} from "./actionsTypes";
import {PUT_CONTACT_ERROR} from "./actionsTypes";
import {PUT_CONTACT_SUCCESS} from "./actionsTypes";
import {PUT_CONTACT_REQUEST} from "./actionsTypes";

export const contactsRequest = () => {
    return {type: FETCH_CONTACTS_REQUEST};
};

export const contactsSuccess = (contacts) => {
    return {type: FETCH_CONTACTS_SUCCESS, contacts};
};

export const contactsError = (error) => {
    return {type: FETCH_CONTACTS_ERROR, error};
};

export const contactRequest = () => {
    return {type: FETCH_CONTACT_REQUEST};
};

export const contactSuccess = (contact) => {
    return {type: FETCH_CONTACT_SUCCESS, contact};
};

export const contactError = (error) => {
    return {type: FETCH_CONTACT_ERROR, error};
};

export const addContactRequest = () => {
    return {type: ADD_CONTACT_REQUEST};
};

export const addContactSuccess = () => {
    return {type: ADD_CONTACT_SUCCESS};
};

export const addContactError = (error) => {
    return {type: ADD_CONTACT_ERROR, error};
};

export const deleteContactRequest = () => {
    return {type: DELETE_CONTACT_REQUEST};
};

export const deleteContactSuccess = () => {
    return {type: DELETE_CONTACT_SUCCESS};
};

export const deleteContactError = (error) => {
    return {type: DELETE_CONTACT_ERROR, error};
};

export const editContactRequest = () => {
    return {type: PUT_CONTACT_REQUEST};
};

export const editContactSuccess = () => {
    return {type: PUT_CONTACT_SUCCESS};
};

export const editContactError = (error) => {
    return {type: PUT_CONTACT_ERROR, error};
};

export const editContact = (contactId, contact, history) => {
    return async dispatch => {
        try{
            dispatch(editContactRequest());
            await axiosContacts.put('/contacts/'+ contactId +'.json', contact);
            dispatch(editContactSuccess());
            history.push("/");
        }
        catch (e) {
            dispatch(editContactError(e));
        }
    };
};

export const deleteContact = (contactId) => {
    return async dispatch => {
        try{
            dispatch(deleteContactRequest());
            await axiosContacts.delete('/contacts/'+ contactId + ".json");
            dispatch(deleteContactSuccess());
            dispatch(fetchContacts());
        }
        catch (e) {
            dispatch(deleteContactError(e));
        }
    };
};

export const postContact = (contact, history) => {
    return async dispatch => {
        try{
            dispatch(addContactRequest());
            await axiosContacts.post('/contacts.json', contact);
            dispatch(addContactSuccess());
            history.push("/");
        }
        catch (e) {
            dispatch(addContactError(e));
        }
    };
};


export const fetchContacts = () => {
    return async dispatch => {
        try{
            dispatch(contactsRequest());
            const response = await axiosContacts.get('/contacts.json');
            dispatch(contactsSuccess(response.data));
        }
        catch (e) {
            dispatch(contactsError(e));
        }
    };
};


export const fetchContact = (contactId) => {
    return async dispatch => {
        try{
            dispatch(contactRequest());
            const response = await axiosContacts.get('/contacts/' + contactId +'.json');
            dispatch(contactSuccess({...response.data, contactId}));
        }
        catch (e) {
            dispatch(contactError(e));
        }
    };
};